// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// lazy cache implementation
int CACHE[CACHE_SIZE];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// -------------
// cycle_length
// -------------

int cycle_length (uint64_t n) {
    assert(n > 0);
    int c = 1;

    if (n < CACHE_SIZE && CACHE[n] != 0)
        c = CACHE[n];
    else {
        if ((n % 2) == 0)
            c = 1 + cycle_length(n >> 1);
        else {
            // optimization: (3n + 1) / 2 == n + (n >> 1) + 1
            // 3n+1 will always end up even, so perform two steps ahead
            c = 2 + cycle_length(n + (n >> 1) + 1);
        }
        if (n < CACHE_SIZE)
            CACHE[n] = c;
    }

    assert(c > 0);
    return c;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    CACHE[1] = 1;
    // input not guaranteed to be i < j
    int start = min(i, j);
    int end = max(i, j);

    // optimization: "middle line", or that when values start, end are positive:
    // if m = end / 2 + 1 and start < m, then
    // collatz_eval(start, end) == collatz_eval(start, m)
    int middle = end / 2 + 1;
    if (start < middle) {
        start = middle;
    }

    int max_cycle_length = 0;
    for (int n = start; n <= end; ++n) {
        int c = cycle_length(n);
        if (c > max_cycle_length)
            max_cycle_length = c;
    }
    return max_cycle_length;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
